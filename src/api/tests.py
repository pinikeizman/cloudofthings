from django.test import TestCase
from .models import Device
from rest_framework.test import APIClient
from django.urls import reverse
from . import consts

class GetDeviceTestCase(TestCase):
    """This class defines the test suite for the device model."""
    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        Device.objects.all().delete()
        self.device = Device(device_name="testName",device_type="testType")
        self.device.save()

    def test_get_devices(self):
        """Test the bucketlist model can create a bucketlist."""
        self.response = self.client.post('/devices/')
        self.assertNotEqual(self.response.data, 
        [
            {
                "device_name": "testName",
                "device_type": "testType"
            }
        ]
    )

class AddDeviceTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()

    def test_api_can_create_a_valid_device(self):
        """Add vaild device test."""
        self.device_data = {
            "device_name": "test_name",
            "device_type": "test_type"
        }
        self.response = self.client.post(
            '/devices/',
            self.device_data,
            format="json")
        self.assertEqual(self.response.status_code, 200)
        self.assertEqual(self.response.data, consts.success_body(consts.DEVICE_ADDED_SUCCESS_MSG) )
    
    def test_api_duplicate_device_name_return_error(self):
        """Add duplicate device name test."""
        self.device_data = {
            "device_name": "test_name",
            "device_type": "test_type"
        }
        self.response = self.client.post(
            '/devices/',
            self.device_data,
            format="json")
        self.response = self.client.post(
            '/devices/',
            self.device_data,
            format="json")
        self.assertEqual(self.response.status_code, 400)
        self.assertEqual(
            self.response.data,
            consts.errors_body(self.device_data["device_name"] + ', ' + consts.DEVICE_NAME_EXISTS_ERR_MSG) )
    
    def test_api_empty_device_name_return_error(self):
        """Add device with empty name test."""
        self.device_data = {
            "device_name": "",
            "device_type": "test_type"
        }
        self.response = self.client.post(
            '/devices/',
            self.device_data,
            format="json")
        self.assertEqual(self.response.status_code, 400)

    def test_api_empty_device_type_return_error(self):
        """Add device with empty type test."""
        self.device_data = consts.DEFAULT_DEVICE
        self.response = self.client.post(
            '/devices/',
            self.device_data,
            format="json")
        self.assertEqual(self.response.status_code, 400)

    def test_api_bad_json_error(self):
        """Add device with bad body format."""
        self.device_data = {}
        self.response = self.client.post(
            '/devices/',
            self.device_data,
            format="json")
        self.assertEqual(self.response.status_code, 400)
    