"""
    Error consts
"""
FIELD_VALIDATION_ERR_MSG = 'field is require with max length of 50.'
DATA_STORE_DOWN_ERR_MSG = 'Data store is down.'
DEVICE_NAME_EXISTS_ERR_MSG = 'Device name is allready our records.'
API_CONSUME_ERR_MSG = 'Api consume application/json only.'
"""
    Success consts
"""
DEVICE_ADDED_SUCCESS_MSG = 'DEVICE ADDED SUCCESSFULLY'
"""
    Defualt objects
"""
DEFAULT_DEVICE = {
    "device_name": "test_name",
    "device_type": ""
}
"""
    Response body
"""
def errors_body(msgs):
    return {
        "errors": msgs
    }
def success_body(msgs):
    return {
        "message": msgs
    }