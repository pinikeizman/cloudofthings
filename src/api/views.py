# Hello World view dependencies
from django.shortcuts import render
from django.http import HttpResponse,HttpRequest
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
# Device view dependencies
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request
# local dependencies
from .serializers import DevicelistSerializer
from .models import Device
import requests 
import json
#consts
from . import consts

def validate(device):
    if not( isinstance(device["device_name"],str) and len(device["device_name"]) > 0 and len(device["device_name"]) < 30):
        raise ValidationError("device_name " + consts.FIELD_VALIDATION_ERR_MSG)
    if not( isinstance(device["device_type"],str) and len(device["device_type"]) > 0 and len(device["device_type"]) < 30):
        raise ValidationError("device_type " + consts.FIELD_VALIDATION_ERR_MSG)

def index(request):
    request = requests.get("http://localhost:3000")
    return HttpResponse(request.text)

class ListDevices(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request):
        """
        Return a list of all devices.
        """
        try:
            devices = Device.objects.all()
            serializer = DevicelistSerializer(devices,many=True)
            return Response(serializer.data)
        except:
            return Response(consts.errors_body(consts.DATA_STORE_DOWN_ERR_MSG))

    def post(self, request):
        """
        Return a list of all devices.
        """
        try:
            data = json.loads(request.body)
            validate(data)
            device = Device(device_name=data["device_name"],device_type=data["device_type"])
            res = device.save()
            return Response(
                consts.success_body(consts.DEVICE_ADDED_SUCCESS_MSG),
                status=status.HTTP_200_OK)
        except IntegrityError:
            return Response(
                consts.errors_body(data["device_name"] + ', ' + consts.DEVICE_NAME_EXISTS_ERR_MSG),
                status=status.HTTP_400_BAD_REQUEST
            )
        except ValidationError as err:
            return Response(
                consts.errors_body('\n '.join(err.messages)),
                status=status.HTTP_400_BAD_REQUEST
            )
        except ValueError:
            return Response(
                consts.errors_body('Device json format: ' + json.dumps(consts.DEFAULT_DEVICE, sort_keys=True, indent=4)),
                status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            return Response(
                consts.errors_body('Device json format: ' + json.dumps(consts.DEFAULT_DEVICE, sort_keys=True, indent=4)),
                status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(str(e))



