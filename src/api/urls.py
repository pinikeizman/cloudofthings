from django.urls import path, re_path
from . import views
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from .views import ListDevices
from django.views.generic import TemplateView

 
import devices.settings as settings

urlpatterns = []
# if we are in debug mode added the react development to main path  
if settings.DEBUG:
    urlpatterns.append( path('', views.index) )

# REST api path
urlpatterns.append( path('devices/',views.ListDevices.as_view()) )