from rest_framework import serializers
from .models import Device

class DevicelistSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Device
        fields = ('device_name', 'device_type')
