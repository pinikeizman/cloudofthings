from django.db import models
from rest_framework import serializers

class Device(models.Model):
    """This class represents the device model."""
    device_name = models.CharField(max_length=50, blank=False, unique=True)
    device_type = models.CharField(max_length=50, blank=False, unique=False)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format("device_name: " + self.device_name + "\ndevice_type: " + self.device_type)