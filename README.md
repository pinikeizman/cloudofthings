# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains a Web Application fornt-end and back-end run with Docker container.
* The project backend written in python3 using Django2 and django-rest-freamwork exposing the following api:
  
    - Path:   /devices
        - Method: GET
        - Responses:
            - 200 success : (array of devices) [{device_name: "test", device_type: "test"}]
    - Path:   /devices
        - Method: POST
        - Body: device to add {device_name: "test", device_type: "test"}
        - Responses:
            - 200: (success message) {"message":"DEVICE ADDED SUCCESSFULLY"}
            - 400: (bad request)     {"errors":"pinitext, Device name is allready our records."}
            - 400: (bad json format) {"errors":"Device json format: {\n    \"device_name\": \"test_name\",\n    \"device_type\": \"\"\n}"}
            - 400: (bad inputs) {"errors":"device_name field is require with max length of 50."}
  
The project sources can be found under /src

* The front-end project is writen in React, project source can be found under /frontend.

* Version 1.0

### How do I get set up? ###

* clone this repository
* run cmd `docker-compose up` be sure to have dokcer and docker-compose installed.
* navigate to http://localhost/app to get the frontend app.
