import React, {Component} from "react";
import {ErrorLabel, FlexBox, Paper, StyledButton, StyledHeader, StyledInput, StyledLabel} from "../Styled/styled";
import _ from "lodash";
import Popover from "material-ui/Popover/Popover";
import Snackbar from "material-ui/Snackbar";
import styled from 'styled-components';

const StyledTable = styled.table`
margin-top:25px;
min-width: 500px;
font-family: Roboto;
border-collapse: collapse;

th,td{
  font-size: 14px;
  font-family: Roboto;
  line-height:28px;
  padding:0px 5px;
}
th{
  font-weight: bold;
}
td{
  font-size: 14px;
}
thead tr{
  background: #EEE;
}
tbody tr:nth-child(2n){
  background: #aed3ff4f;
}
`

class ShowDevices extends Component {

    state = {
    };

    componentDidMount(){
        fetch('/devices')
        .then(response=>response.json())
        .then(data=>this.setState({devices:data}))
    }

    tableHeadGenerator = (csvTableHeadList = []) =>
        ( <tr key={ _.uniqueId() }>
        {
            csvTableHeadList.map(csvTableHeadItem =>
            <th key={ _.uniqueId() }>{csvTableHeadItem}</th>)
        }
        </tr> );
    tableBodyGenerator = (csvTableBody = []) =>
        csvTableBody.map( csvTableBodyLine =>
            <tr key={ _.uniqueId() }>
            {
                Object.keys(csvTableBodyLine).map(key => <td key={ _.uniqueId() }>{csvTableBodyLine[key]}</td>)
            }
            </tr>);

    render() {
        const {errors} = this.state
        return ( 
        <FlexBox flexDirection='column' justifyContent='center' alignItems='center'>
            <StyledHeader>Show All Devices</StyledHeader>
            <StyledTable>
                <thead>
                {
                    this.tableHeadGenerator(["Device Name","Device Type"])
                }
                </thead>
                <tbody>
                {
                this.tableBodyGenerator(this.state.devices)
                }
                </tbody>
            </StyledTable> 
        </FlexBox>);
        
    }
}

export default ShowDevices;
