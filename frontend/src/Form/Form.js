import React, {Component} from "react";
import {ErrorLabel, FlexBox, Paper, StyledButton, StyledHeader, StyledInput, StyledLabel} from "../Styled/styled";
import _ from "lodash";
import Snackbar from "material-ui/Snackbar";


const supportEmail = require('../../package.json').config.supportEmail;

const initFormValues = {
    deviceName: "",
    deviceType: ""
}
class Form extends Component {

    state = {
        formValues: {...initFormValues},
        errors: [],
        snackbar: {}
    };

    formFields = {
        deviceName: "Device Name",
        deviceType: "Device Type"
    };

    validate = (formValues) => {

        const ERRORS = {
            EMPTY_FIELD_ERROR: "This field is required.",
            ONLY_LETTERS_ERROR: "This field can contains only letters.",
            ONLY_NUMBERS_ERROR: "This field can contains only numbers.",
            MIN_2_CHARS_ERROR: "This filed value must be at least 2 character.",
            EXACTLY_10_CHARS_ERROR: "This filed value must be exectly 10 character.",
            NOT_VAILD_EMAIL_ERROR: "This filed value must be a valid email address."
        };

        const validator = {
            notEmptyString: {
                regex: {
                    test: (value) => value !== ""
                },
                error: ERRORS.EMPTY_FIELD_ERROR
            },
            onlyLetters: {
                regex: /[a-zA-Z]+/,
                error: ERRORS.ONLY_LETTERS_ERROR
            },
            email: {
                regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                error: ERRORS.NOT_VAILD_EMAIL_ERROR
            },
            onlyNumbers: {
                regex: /[0-9]+/,
                error: ERRORS.ONLY_NUMBERS_ERROR
            },
            minimum2Chars: {
                regex: {
                    test: (value) => value.length >= 2
                },
                error: ERRORS.MIN_2_CHARS_ERROR
            },
            exactly10Chars: {
                regex: {
                    test: (value) => value.length == 10
                },
                error: ERRORS.EXACTLY_10_CHARS_ERROR
            }
        };

        const filedToValidatorList = {
            deviceName: ["notEmptyString"],
            deviceType: ["notEmptyString"],

        }

        // this function gets validator name and  field value and validate the value
        const generigValidator = (validatorName, fieldValue) => {
            if (validator[validatorName].regex.test(fieldValue)) {
                return undefined;
            }
            return validator[validatorName].error;
        }

        let errors = {};

        Object.keys(formValues).forEach(field => {
                filedToValidatorList[field].forEach(validatorName => {
                        const error = generigValidator(validatorName, formValues[field]);
                        error ? _.set(errors, field, [...( errors[field] || [] ), error]) : errors[field];
                    }
                );
            }
        );
        return errors;
    }

    handleOnChange = (field, value) => {
        let errors = this.validate({...this.state.formValues, ...{[field]: value}});
        this.setState((prevState) => ({
            formValues: _.merge(prevState.formValues, {[field]: value}),
            lastField: field,
            errors
        }));
    }

    // this function set the current focused input in state and position
    // the Popover with the errors
    handleInputFocus = (e, fieldName) => {
        this.setState({
            lastField: fieldName,
            anchorEl: this[fieldName]
        });
    }

    // this function is onClose handler for the snackbar
    // sets snackbar open propetry to false.
    handleSnackbarClose = () => {
        this.setState({
            snackbar: {open: false}
        });
    }

    // this function handle onClick on the submit button
    submit = () => {
        const errors = this.validate(this.state.formValues);

        // dont submit the form if the error object isn't empty.
        if (!_.isEmpty(errors)) {
            this.setState({
                errors
            })
            return;
        }

        // send mail with api
        fetch("/devices/",{
            method:"POST",
            body:JSON.stringify({
                device_name: this.state.formValues.deviceName,
                device_type: this.state.formValues.deviceType
            })
        }).then((response)=>{
            if((response.status != 200))
                    throw response.json()
            else
                return response.json()
        }).then(data => {
            this.setState({
                snackbar:{
                    open: true,
                    message: data.message
                },
                formValues: {...initFormValues}
            })
        }).catch(err => {
            err.then(errors => this.setState({
                errors
            }));
        });
    }

    render() {
        const {errors} = this.state

        return (
            <FlexBox alignItems="center" justifyContent="center" flexDirection="column" style={{padding: 25}}>
                <StyledHeader style={{marginBottom: 30}}>Add Device</StyledHeader>
                {
                    Object.keys(this.formFields).map((fieldName, i) => (

                        <FlexBox key={fieldName}>
                            <StyledLabel>
                                {this.formFields[fieldName]}
                            </StyledLabel>
                            <StyledInput
                                style={ errors[fieldName] ? {border: '1px solid red'} : {} }
                                tabIndex={i + 1}
                                key={fieldName}
                                value={this.state.formValues[fieldName] || ""}
                                onFocus={(e) => this.handleInputFocus(e, fieldName)}
                                onChange={(e) => this.handleOnChange(fieldName, e.target.value)}
                                onRequestClose={this.onRequestClose}
                                type="text">
                            </StyledInput>
                        </FlexBox>

                    ))
                }
                <FlexBox justifyContent="right">
                    <StyledButton style={{width: 200}} onClick={() => 
                        this.submit()
                    }>
                        Submit
                    </StyledButton>
                </FlexBox>
                
                <ul>
                    {
                        Object.keys(this.state.errors || {}).map(error=><li key={_.uniqueId}>
                            {
                             this.formFields[error] ? this.formFields[error] + ": " + this.state.errors[error] : this.state.errors[error]
                            }</li>)
                    }
                </ul>
                <Snackbar
                    open={this.state.snackbar.open || false}
                    message={this.state.snackbar.message || ""}
                    autoHideDuration={4000}
                    onRequestClose={this.handleSnackbarClose}
                />
            </FlexBox>
        );
    }
}

export default Form;
