import React from "react";
import ReactDOM from "react-dom";
import "./index.sass";
import App from "./App/App";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

ReactDOM.render(
    <MuiThemeProvider>
        <App/>
    </MuiThemeProvider>
    , document.getElementById('root'));
