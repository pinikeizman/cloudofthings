import React, {Component} from 'react';
import styled from 'styled-components';
import {FlexBox} from '../Styled/styled';

const TabHeader = (props) => {
    const TabHeaderComponent = styled.div`
        transition: background-color 350ms ease;
        font-family:Roboto;
        flex: 1;
        cursor:pointer;
        color:white;
        font-size:28px;
        line-height: 40px;
        height:40px;
        text-align: center;
        margin: 5px;
        background-color:#BDC2C7;
        border-radius: 5px;
        text-shadow: 0px 0px 3px #333;
        :hover{
            background-color:#04499B;
        }
        &.active{
            background-color:#04499B;
        }
    `;
    return (
        <TabHeaderComponent
            onClick={props.onClick}
            style={props.style}
            className={"tab-header " + props.className}>
            <label className="title">
                {props.title}
            </label>
        </TabHeaderComponent>
    )
};

class TabComponent extends Component {

    state = {
        selectedTab: this.props.tabs[0].title
    };

    handleTabChange = (tabTitle) => {
        this.setState({selectedTab: tabTitle});
    };

    render() {
        return (
            <FlexBox flexDirection='column'>
                <FlexBox>
                    {
                        this.props.tabs.map((tab) => <TabHeader
                            onClick={() => this.handleTabChange(tab.title)}
                            className={this.state.selectedTab == tab.title ? "active" : ""}
                            key={tab.title}
                            title={tab.title}/>)
                    }
                </FlexBox>
                    {
                        this.props.tabs.find((tab) => tab.title == this.state.selectedTab).component
                    }
            </FlexBox>
        )
    }

}

export default TabComponent;
