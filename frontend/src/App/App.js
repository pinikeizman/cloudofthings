import React, {Component} from "react";
import {FlexBox} from "../Styled/styled";
import ShowDevices from "../ShowDevices/index";
import Form from "../Form";
import TabComponent from "../TabComponent/TabComponent"

class App extends Component {

    tabs = [
        {
            title: "Show Devices",
            component: <ShowDevices/>
        },
        {
            title: "Add Device",
            component: <Form/>

        }
    ];

    render() {
        return (
            <TabComponent tabs={this.tabs}/>
        );
    }
}

export default App;
