# README #

### What is this repository for? ###

Jones frontend developer interview task.
This is a single page application which display a form with the values:

- First Name
- Last Name
- Email Address
- Phone

After clicking submit the form will send an email using https://app.sendgrid.com/ api
with the following format:
from: "BOT@getjones.com"
to: <package.json -> config.supportEmail property value>
subject: "New Lead"
content:
  First Name: <form first name value>
  Last Name: <form last name value>
  Email Address: <form email address value>
  Phone: <form phone value>


### How do I get set up? ###

- please for your own safety use chrom browser, you can get it here: https://www.google.com/chrome/
- install chrom cors allow extension from here: https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi
- verify that the plugin is enabled, watch this: https://www.youtube.com/watch?v=ZAn7hCA21BU
- then you can clone the repository from: https://bitbucket.org/pinikeizman/jones.git
- change the email address in package.json config.supportEmail to match the support email address (the address which the emails will be send to.)
- hit npm start to lunch the app at: http://localhost:3000


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
