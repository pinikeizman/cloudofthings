# start from latest ubuntu image
FROM ubuntu
# update apt-get
RUN apt-get update
# insatll apache2
RUN apt-get install -y apt-utils vim curl apache2 apache2-utils
# install python3
RUN apt-get -y install python3 libapache2-mod-wsgi-py3
RUN ln /usr/bin/python3 /usr/bin/python
# install pip
RUN apt-get -y install python3-pip
# pip3 aliasing to pip
RUN ln /usr/bin/pip3 /usr/bin/pip
RUN pip install --upgrade pip
# copy pip freeze file
ADD ./requirements.txt .
# install dependencies
RUN pip install -r requirements.txt
# copy site conf file to appache folder.
ADD ./demo_site.conf /etc/apache2/sites-available/000-default.conf
# copy all artifacts
ADD . /var/www/html/django
# set user and file permissions
RUN chown www-data /var/www/html/django/src/db.sqlite3
RUN chmod 777 var/www/html/django/src/ -R
# expose 80
EXPOSE 80 
# run apache in deamon mode
CMD ["apache2ctl", "-D", "FOREGROUND"]
